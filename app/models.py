# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
def report_directory_path(instance, filename): 
    return 'report_{0}/{1}'.format(instance.user.id, filename)

class LaporanMasuk(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    subject = models.CharField(max_length=255, null=False)
    lat = models.FloatField(null=True, blank=True)
    longs = models.FloatField(null=True, blank=True)
    keterangan = models.TextField(null=False)
    status = models.CharField(max_length=50)
    sentimen = models.CharField(max_length=60)
    create_date = models.DateTimeField(auto_now_add=True)
    create_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="laporans")

class Images(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    images = models.ImageField(upload_to=report_directory_path, blank=True,null=True)
    laporan = models.ForeignKey(LaporanMasuk, on_delete=models.CASCADE, related_name="images")
    create_date = models.DateTimeField(auto_now_add=True)
    create_by = models.ForeignKey(User, on_delete=models.CASCADE)

class Tindakan(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    keterangan = models.TextField(blank=True)
    status = models.CharField(max_length=50)
    laporan = models.ForeignKey(LaporanMasuk, on_delete=models.CASCADE, related_name="tindakans")
    process_date = models.DateTimeField(auto_now_add=True)
    process_by = models.ForeignKey(User, on_delete=models.CASCADE)

class entity(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    value = models.CharField(max_length=250)


