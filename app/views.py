# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.template import loader
from django.http import HttpResponse
from django import template
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from urllib.parse import urlparse
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib import messages
from django.db import connection
from app.models import *
from app.serializers import *
from rest_framework import viewsets
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.permissions import IsAuthenticated

def landing_page(request):
    context = {}
    context['segment'] = 'index'
    html_template = loader.get_template( 'home/index.html' )
    return HttpResponse(html_template.render(context, request))

class LaporanMasukViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    model = LaporanMasuk
    queryset = LaporanMasuk.objects.all()
    serializer_class = LaporanMasukSerializer

