from management_api.token_utils import encrypt
from django.conf import settings
import json
from datetime import date

def generate_token(id_user, app_id, expired_id):
    password = settings.SECRET_KEY
    data = {
        "id_user":id_user,
        "app_id":app_id,
        "expired_id":expired_id,
        "create_at":str(date.today())
    }
    chiper_text = encrypt(json.dumps(data),password)
    return chiper_text
