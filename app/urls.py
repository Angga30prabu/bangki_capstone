# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from app import views
app_name="app"
urlpatterns = [
    # path('',views.dashboard_client, name="client_dashboard"),
    path('page', views.landing_page, name="home"),
    path('laporan/', views.LaporanMasukViewSet.as_view({"get":"list"}), name="laporan-list"),
    path('laporan/create', views.LaporanMasukViewSet.as_view({"put":"create"}), name="laporan-create"),

]
