from rest_framework import serializers
from app.models import *

class ImagesSerializer(serializers.ModelSerializer):
    # user_detail = UserDetailSerializer(many=False)
    images = serializers.CharField(required=True)
    filename = serializers.CharField(required=True)
    class Meta:
        model = Images
        fields = ('images')

class TindakanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tindakan
        fields = ('keterangan', 'status', 'process_date', 'process_by')

class LaporanMasukSerializer(serializers.ModelSerializer):
    # user_detail = UserDetailSerializer(many=False)
    images = ImagesSerializer(many=True, required=False)
    tindakans = TindakanSerializer(many=True, required=False)
    class Meta:
        model = LaporanMasuk
        fields = ('subject', 'lat', 'longs', 'keterangan', 'images', 'tindakans')
    
    def create(self, validate_data):
        o = LaporanMasuk(**validate_data)
        o.create_by = self.context["request"].user
        o.save()
        return o




