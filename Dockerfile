FROM python:3.7

ENV FLASK_APP run.py
WORKDIR app

COPY requirements.txt .
RUN pip install -r requirements.txt
#COPY . .

#RUN python manage.py makemigrations
#RUN python manage.py migrate
#RUN python manage.py collectstatic --noinput

EXPOSE 5005
#CMD ["gunicorn", "--config", "gunicorn-cfg.py", "-k uvicorn.workers.UvicornWorker", "core.wsgi"]
#CMD ["daphne",  "-b 0.0.0.0", "-p 5005" ,"core.asgi:application"]
