# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from django.conf.urls.static import static
from django.conf import settings
from django.views.static import serve 
def view_404(request, exception=None):
    # make a redirect to homepage
    # you can use the name of url or just the plain link
    return redirect('/home') # or redirect('name-of-index-url')

urlpatterns = [
    # path('admin/', admin.site.urls),
    path("", include("authentication.urls")),  # add this
    path("app/", include("app.urls"), name="app"),  # add this
    path("admin/", include("admins.urls"), name="admin"),
    path('api-auth/', include('rest_framework.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 
if not settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# handler404 = view_404