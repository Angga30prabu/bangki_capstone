from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from management_api.serializer import SendImage,SendText
from management_api.interface import send_text, get_msg_history
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework import authentication, permissions
from authentication.authentication import TokenAuthentication
from rest_framework.decorators import api_view

# @csrf_exempt
@api_view(["GET","POST"])
def api_send_text(request):
    session_id = request.META.get("HTTP_X_SESSIONID")
    if not session_id:
        return JsonResponse({"msg":"header X-SESSIONID not valid"}, status=400)
    if request.method == "POST":
        data = JSONParser().parse(request)
        obj = SendText(data=data)
        if obj.is_valid():
            resp = send_text(settings.API_WA, session_id,obj.data["text"],obj.data["to_no"],None)
            print(resp)
            return JsonResponse(resp.json(), status=resp.status_code)

    return JsonResponse({"msg":"method not allowes"}, status=400)

class ApiSendFile(APIView):
    parser_classes = (MultiPartParser,FileUploadParser)
    # authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.AllowAny,)
    def post(self, request, *args, **kwargs):
        session_id = request.META.get("HTTP_X_SESSIONID")
        if not session_id:
            return JsonResponse({"msg":"header X-SESSIONID not valid"}, status=400)
        if request.method == "POST":
            obj = SendImage(data=request.data)
            
            if obj.is_valid():
                print(obj.data["files"])
                resp = send_text(settings.API_WA, session_id,obj.data["text"],obj.data["to_no"],files=request.FILES["files"])
                print(resp)
                return JsonResponse(resp.json(), status=resp.status_code)
            print(obj.errors)
        return JsonResponse({"msg":"method not allowed"}, status=400)
@api_view(["GET"])
def api_get_msg(request):
    session_id = request.META.get("HTTP_X_SESSIONID")
    if not session_id:
        return JsonResponse({"msg":"header X-SESSIONID not valid"}, status=400)
    if request.method == "GET":
        page = request.GET.get("page")
        size = request.GET.get("size")
        start_data = request.GET.get("start_date")
        resp = get_msg_history(settings.API_WA, session_id,page, size, start_data)
        print(resp)
        return JsonResponse(resp.json(), status=resp.status_code)
    return JsonResponse({"msg":"method not allowes"}, status=400)
            
# def api_get
