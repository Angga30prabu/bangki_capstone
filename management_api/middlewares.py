import socket
import time
from app.models import ApplicationModel
from management_api.models import TrafikApi
from django.utils.deprecation import MiddlewareMixin

class RequestLogMiddleware(MiddlewareMixin):
    def __init__(self, *args, **kwargs):
            """Constructor method."""
            super().__init__(*args, **kwargs)
    def process_request(self, request):
        request.start_time = time.time()

    def process_response(self, request, response):
        if request.META.get("HTTP_AUTHORIZATION"):
            app = ApplicationModel.objects.filter(user_id=request.user).first()
            TrafikApi(
                app_id=app,
                jumlah_trafik=1,
                remote_address=request.META['REMOTE_ADDR'],
                server_name=socket.gethostname(),
                request_method=request.method,
                request_path=request.get_full_path(),
                response_status=response.status_code,
                response_time=time.time() - request.start_time,
                ).save()

        # save log_data in some way

        return response