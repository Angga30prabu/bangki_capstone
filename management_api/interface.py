import requests


def create_session(url, sessionId, call_msg, call_status, call_chat):
    url = "{}/admin/account/create".format(url)
    payload = {
    'sessionId': sessionId,
    'callbackNewMsg': call_msg,
    'callbackStatus': call_status,
    'callbackChatStatus': call_chat}
    print(payload)
    headers = {
    'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, json = payload)
    return response

def send_text(url, sessionid, text, to_no, files):
    url = "{}/message/{}/send_msg".format(url, sessionid)
    print(files)
    payload = {'text': text,
    'to_no': to_no}
    file = []
    if files:
        file.append(files)
        print(file)
    headers= {}
    response = requests.request("POST", url, headers=headers, data = payload)
    return response

def get_msg_history(url,sessionid, page, size, start_data):
    url = "{}/message/{}?page={}&size={}&start_date={}".format(url, sessionid, page, size, start_data)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response

def get_list_account(url,sessionid, page, size, start_data):
    url = "{}/admin/account?page={}&size={}".format(url, page, size)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response

def delete_account(url, sessionid):
    url = "{}/admin/account/{}/delete".format(url, sessionid)
    payload = {}
    headers= {}
    response = requests.request("DELETE", url, headers=headers, data = payload)
    return response

def login_session(url, sessionid):
    url = "{}/account/{}/login".format(url, sessionid)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response

def evade_conflict_session(url, sessionid):
    url = "{}/account/{}/activate".format(url, sessionid)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response

def pause_session(url, sessionid):
    url = "{}/account/{}/pause".format(url, sessionid)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response

def resume_session(url, sessionid):
    url = "{}/account/{}/resume".format(url, sessionid)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response

def logout_session(url, sessionid):
    url = "{}/account/{}/logout".format(url, sessionid)
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    return response



