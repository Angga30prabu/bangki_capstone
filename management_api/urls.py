from django.urls import path
from .views import api_send_text, ApiSendFile, api_get_msg

app_name="api"
urlpatterns = [
    path('send_text', api_send_text, name="send_text"),
    path('send_file', ApiSendFile.as_view(), name="send_file"),
    path('get_message', api_get_msg, name="get_msg"),
]
