from django.db import models
from app.models import ApplicationModel
from django.utils import timezone
# Create your models here.

class TrafikApi(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    app_id = models.ForeignKey(ApplicationModel, on_delete=models.CASCADE)
    date_trafik = models.DateField(auto_now_add=True)
    jumlah_trafik = models.IntegerField()
    remote_address = models.CharField(max_length=255)
    server_name = models.CharField(max_length=255)
    request_method = models.CharField(max_length=255)
    request_path = models.CharField(max_length=255)
    response_status = models.CharField(max_length=255)
    response_time = models.FloatField()
    


class ThrottleModel(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    app_id = models.ForeignKey(ApplicationModel, on_delete=models.CASCADE)
    hit = models.IntegerField()
    date = models.DateField()
