from rest_framework import serializers

class SendText(serializers.Serializer):
    to_no = serializers.CharField(required=True)
    text = serializers.CharField(required=True)

class SendImage(serializers.Serializer):
    to_no = serializers.CharField(required=True)
    text = serializers.CharField(required=True)
    files = serializers.FileField()

    