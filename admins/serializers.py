from rest_framework import serializers
from admins.models import UserDetail

class UserDetailSerializer(serializers.ModelSerializer):
    profile_image = serializers.CharField(max_length=255)
    class Meta:
        model = UserDetail
        fields = ('profile_image')