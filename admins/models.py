from django.db import models
from django.contrib.auth.models import User

# Create your models here.
def user_directory_path(instance, filename): 
    return 'user_{0}/{1}'.format(instance.user.id, filename)

class UserDetail(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    profile_image = models.ImageField(upload_to=user_directory_path, blank=True,null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_detail")
    role = models.CharField(max_length=50, choices=[("user","User"), ("admin", "Admin")], default="user")
