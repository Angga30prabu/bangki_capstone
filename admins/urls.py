# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from admins import views

app_name="admins"

urlpatterns = [

    path('', views.index, name='home'),
    path('users', views.user_page, name="page_user"),
    path('users/<pk>/update', views.UserUpdateView.as_view(), name="update_user"),
    path('users/<pk>/profile', views.UserProfileUpdateView.as_view(), name="update_profile"),
    path('laporan/', views.list_compliant, name="laporan")
]
