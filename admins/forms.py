from django import forms
from django.contrib.auth.models import User
from admins.models import UserDetail

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['email'].disabled = True
        self.fields['username'].disabled = True
    class Meta:
        model = User
        fields = ["username","first_name","last_name","email","password"]
        required = ["first_name","last_name"]
class UserDetailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserDetailForm, self).__init__(*args, **kwargs)
        
        if self.instance.profile_image.name is not None:
            self.fields['profile_image'].disabled = True
        
    class Meta:
        model = UserDetail
        fields = ["profile_image"]

