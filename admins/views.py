# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
import django_tables2 as tables
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.template import loader
from django.http import HttpResponse
from django import template
from django.contrib.auth.models import User
from django.views.generic.edit import FormView, UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from admins.forms import UserForm, UserDetailForm
from admins.models import UserDetail
from urllib.parse import urlparse
from datetime import datetime, timedelta
from app.models import LaporanMasuk
# from rest_framework.views import ListView


@login_required(login_url="/login/")
def index(request):
    context = {}
    context['segment'] = 'index'
    html_template = loader.get_template( 'admin/index.html' )
    print(request.user.is_superuser)
    return HttpResponse(html_template.render(context, request))


class ListComplaintTable(tables.Table):
    id = tables.Column()
    class Meta:
        model = LaporanMasuk
        template_name = "django_tables2/bootstrap.html" 
        fields = [""]

@login_required(login_url="/login/")
def list_compliant(request):
    context = {}
    obj = LaporanMasuk.objects.all()
    context['segment'] = 'list_complaint'
    context['object_list'] = ListComplaintTable(obj)
    html_template = loader.get_template( 'admin/list_complaint.html' )
    
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def user_page(request):
    context = {}
    context['segment'] = 'user_page'
    context["data"] = User.objects.filter(is_superuser=False).all()
    html_template = loader.get_template( 'admin/user_pages.html' )
    return HttpResponse(html_template.render(context, request))





@login_required(login_url="/login/")
def invoice_page(request):
    context = {}
    context['segment'] = 'invoice_page'

    html_template = loader.get_template( 'admin/invoice.html' )
    return HttpResponse(html_template.render(context, request))


class UserUpdateView(LoginRequiredMixin,UpdateView):
    model = User
    form_class = UserForm
    template_name = "forms/form.html"
    success_url = "#"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["name_form"] = "Update User"
        return context
    def form_valid(self, form):
        form.instance.set_password(form.instance.password)
        form.instance.create_by = self.request.user
        return super().form_valid(form)

class UserProfileUpdateView(LoginRequiredMixin,UpdateView):
    model = UserDetail
    form_class = UserDetailForm
    template_name = "forms/form.html"
    success_url = "#"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["name_form"] = "Update Profile"
        context["file_attach"] = True
        return context
    def get_object(self, queryset=None):
        return self.model.objects.get(user=self.request.user)
    def form_valid(self, form):
        print(form)
        form.instance.create_by = self.request.user
        return super().form_valid(form)