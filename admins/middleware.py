from django.urls import reverse
from django.shortcuts import redirect
from django.http import Http404


class RestrictUserMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path.find("/admin/") > -1:  # restricted admin url for custom admin site
            if not request.user.is_superuser:
                raise Http404
        response = self.get_response(request)
        return response
