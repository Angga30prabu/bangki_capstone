# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.forms.utils import ErrorList
from django.http import HttpResponse
from .forms import LoginForm, SignUpForm
from admins.models import UserDetail
from django.contrib.auth.models import User
from django.views.generic.edit import FormView, UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from admins.forms import UserForm, UserDetailForm
from admins.models import UserDetail
from urllib.parse import urlparse
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings
from django.utils.http import int_to_base36
from post_office import mail
from django.template.loader import render_to_string
from django.utils.http import base36_to_int
from django.contrib.auth.tokens import default_token_generator
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from rest_framework import viewsets
from authentication.serializers import *
from rest_framework.response import Response

def send_email_activation(user):
    data = {
        'token': default_token_generator.make_token(user),
        'uid': int_to_base36(user.id),
        'host': settings.DOMAIN,
        'user': user,
        'email_title': 'Aktivasi Akun'
    }

    send = mail.send(
        [user.email],
        settings.DEFAULT_FROM_EMAIL,
        subject='Aktivasi Akun',
        html_message=render_to_string('email.html', context=data)
    )
    return send


def login_view(request):
    form = LoginForm(request.POST or None)

    msg = None

    if request.method == "POST":

        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                if user.is_superuser:
                    print("login succeess")
                    return redirect("/admin")
            else:    
                msg = 'Invalid credentials'    
        else:
            msg = 'Error validating the form'    
        print(msg)
    return render(request, "accounts/login.html", {"form": form, "msg" : msg})

def register_user(request):
    msg     = None
    success = False
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            
            return redirect("/login/")

        else:
            msg = 'Form is not valid'    
    else:
        form = SignUpForm()

    return render(request, "accounts/register.html", {"form": form, "msg" : msg, "success" : success })


class UserUpdateView(LoginRequiredMixin,UpdateView):
    model = User
    form_class = UserForm
    template_name = "forms/form.html"
    success_url = "#"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["name_form"] = "Update User"
        return context
    def form_valid(self, form):
        form.instance.set_password(form.instance.password)
        form.instance.create_by = self.request.user
        return super().form_valid(form)

class UserProfileUpdateView(LoginRequiredMixin,UpdateView):
    model = UserDetail
    form_class = UserDetailForm
    template_name = "forms/form.html"
    success_url = "#"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["name_form"] = "Update Profile"
        context["file_attach"] = True
        return context
    def get_object(self, queryset=None):
        return self.model.objects.get(user=self.request.user)
    def form_valid(self, form):
        print(form)
        form.instance.create_by = self.request.user
        return super().form_valid(form)

class UserViewSet(viewsets.ModelViewSet):
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer



