from rest_framework import serializers
from django.contrib.auth.models import User
from admins.serializers import UserDetailSerializer

class UserSerializer(serializers.ModelSerializer):
    # user_detail = UserDetailSerializer(many=False)
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'user_detail')