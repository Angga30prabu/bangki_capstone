# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path
from .views import login_view, register_user, UserUpdateView, UserProfileUpdateView, UserViewSet
from django.contrib.auth.views import LogoutView
from rest_framework_simplejwt import views as jwt_views

app_name="auth"
urlpatterns = [
    path('login/', login_view, name="login"),
    path('register/', register_user, name="register"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path('users/<pk>/update', UserUpdateView.as_view(), name="update_user"),
    path('users/<pk>/profile', UserProfileUpdateView.as_view(), name="update_profile"),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/users', UserViewSet.as_view({"get":"list"}), name='user_api_list'),
    path('api/users/<pk>', UserViewSet.as_view({"get":"retrieve"}), name='user_api_retrive'),
    path('api/users/register', UserViewSet.as_view({"post":"create"}), name='user_api_create'),
]
